Author: Harry Cheng hcheng5@uoregon.edu

Repo: https://bitbucket.org/harrycheng5/proj7-auth-ux/

Description: It is a web app that can compute the open time and close time for brevet ride based on the control points user has put in. For the rule of brevet, you can check here (https://rusa.org/pages/rulesForRiders).

# Project 7: Adding authentication and user interface to brevet time calculator service

## What is in this repository

In brevet_web there is app.py, which has most of the web functionalities, including flask, api, mongodb, and other login functions. Whereas website folder has consumer program.

## What does it do
It is improved version of web app that is based upon project 4. You can find the basic information here (https://bitbucket.org/harrycheng5/proj4-brevets/).
Other previous improved versions can be found here: profect 5 (https://bitbucket.org/harrycheng5/proj5-mongo/), project 6 (https://bitbucket.org/harrycheng5/proj6-rest/)

Users can get brevet ride information by entering open and close time in web page. With new functionalities users can now register accounts with username and password
to login and logout. To view the APIs of the data, you need to get token, which requires you to login. 

For developers, the knowledge behind login features is flask-login and flask-WTF. flask-login deals with how the register, login, and logout work in the back, whereas flask-WTF provides basic frontend functions in login and register pages.

## How to use new features
There are several new buttons on the top of web page now, which are Home, Register, LogIn, LogOut, and Get token. 
LogOut button only appears when you are already logged in. 
Press Get token button you will be directed to a json object which shows you a token and how long it lasts (5 minutes or 300 seconds). If you havn't logged in and go to Get token, you will be directed to log in page.

After getting a token, you can see the APIs by typing url with ?auth_token="token you get" at the end. For example, http://localhost:5000/listAll?auth_token="token you get". If you do not enter a token or have a wrong one, a error message will be presented.

