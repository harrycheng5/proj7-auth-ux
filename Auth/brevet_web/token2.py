from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import time
import config
from flask import Flask

app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

def generate_auth_token(id, expiration=600):
   s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
   # pass index of user
   return s.dumps({'id': id})

def verify_auth_token(token):
    s = Serializer(app.config['SECRET_KEY'])
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    # valid token, but expired
    except BadSignature:
        return None    # invalid token
    return True

if __name__ == "__main__":
    t = generate_auth_token(1, 10)
    for i in range(1, 20):
       print(str(t))
       print(verify_auth_token(t))
       time.sleep(1)
