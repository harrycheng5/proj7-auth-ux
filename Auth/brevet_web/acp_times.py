"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    if control_dist_km > brevet_dist_km:
        control_dist_km = brevet_dist_km
        
    controls = [200, 200, 200, 400, 300]
    speeds = [34, 32, 30, 28, 26]
    hour = 0
    minute = 0
    
    level = 0
    while control_dist_km > 0:
        h = int(min(control_dist_km, controls[level]) // speeds[level])
        m = round(min(control_dist_km, controls[level]) % speeds[level] / speeds[level] * 60)
        
        control_dist_km -= controls[level]
        level += 1

        hour += h
        minute += m

    hour += minute // 60
    minute = minute % 60

    opening_time = brevet_start_time.shift(hours = +hour)
    opening_time = opening_time.shift(minutes = +minute)
    
    return opening_time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    time_limits = {200: [13, 30],
                   300: [20, 00],
                   400: [27, 00],
                   600: [40, 00],
                   1000: [75, 00]}
    if control_dist_km >= brevet_dist_km:
        closing_time = brevet_start_time.shift(hours = +time_limits[brevet_dist_km][0])
        closing_time = closing_time.shift(minutes = +time_limits[brevet_dist_km][1])
        return closing_time.isoformat()

        
    controls = [200, 200, 200, 400, 300]
    speeds = [15, 15, 15, 11.428, 13.333]
    hour = 0
    minute = 0
    
    level = 0
    while control_dist_km > 0:
        h = int(min(control_dist_km, controls[level]) // speeds[level])
        m = round(min(control_dist_km, controls[level]) % speeds[level] / speeds[level] * 60)
        
        control_dist_km -= controls[level]
        level += 1

        hour += h
        minute += m

    hour += minute // 60
    minute = minute % 60
    
    closing_time = brevet_start_time.shift(hours = +hour)
    closing_time = closing_time.shift(minutes = +minute)

    
    return closing_time.isoformat()
