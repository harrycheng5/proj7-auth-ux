import os
from pymongo import MongoClient
import flask
from flask import Flask, redirect, url_for, request, render_template, flash
import arrow
import acp_times
import config
import logging

import pandas as pd
from forms import LoginForm, RegisterForm
from password import hash_password, verify_password
from token2 import generate_auth_token, verify_auth_token

from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required)
app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

class User(UserMixin):
    def __init__(self, name, id, token, active=True):
        self.name = name
        self.id = id
        self.active = active
        self.token = token

    def is_active(self):
        return self.active

USERS = {}


login_manager = LoginManager()
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"


item_list = []
number = 1

@login_manager.user_loader
def load_user(id):
    return USERS.get(int(id))

login_manager.setup_app(app)

@app.route('/')
@app.route('/index')
def index():
    app.logger.debug('Main page entry')
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('calc.html', items=items)

@app.route('/login', methods=['GET', 'POST'])
def login():
    _datas = db.user.find()
    datas = [data for data in _datas]

    form = LoginForm()
    if form.validate_on_submit():

        for data in datas:
            if form.username.data == data['username']:
                if verify_password(form.password.data, data['password']):
                    token = generate_auth_token(data['id'], 300)
                    user = User(name=data['username'], id=data['id'], token=token, active=True)
                    USERS[data['id']] = user
                    login_user(user, remember=form.remember_me.data)

                    flash("You've logged in successfully! {}".format(data['username']))
                    return redirect(url_for('index'))
                else:
                    flash('Incorrect password')
                    return render_template('login.html',  title='Sign In', form=form), 401

        flash('Unfound username')
        return render_template('login.html',  title='Sign In', form=form), 401
    return render_template('login.html',  title='Sign In', form=form)

@app.route('/api/token')
@login_required
def get_auth_token():
    id = list(USERS.keys())[0]
    token = USERS[id].token
    return flask.jsonify({ 'token': token.decode('ascii'),
                           'duration': 300})

@app.route('/api/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        flash('Register requested for user {}'.format(form.username.data))

        _users = db.user.find()
        id = len([user for user in _users]) + 1

        username = form.username.data
        password = hash_password(form.password.data)
        user_data = {'username': username,
                     'password': password,
                     'id': id}
        db.user.insert_one(user_data)

        return flask.jsonify({ 'username': username }), 201,
        {'Location': url_for('get_user', id = id, _external = True)}
        return redirect(url_for('index')), 201
    return render_template('register.html',  title='Sign Up', form=form), 400

@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You have logged out')
    return redirect(url_for('index'))

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug('Page not found')
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route('/_calc_times')
def _calc_times():
    app.logger.debug('Got a JSON request')

    km = request.args.get('km', 999, type=float)
    mile = request.args.get('mile', type=float)
    brevet_dist = int(request.args.get("brevet_dist", type=str))
    begin_date = request.args.get('begin_date', type=str)
    begin_time = request.args.get('begin_time', type=str)
    date_time = arrow.get(begin_date + ' ' + begin_time, 'YYYY-MM-DD HH:mm')

    app.logger.debug('km={}'.format(km))
    app.logger.debug('request.args: {}'.format(request.args))

    open_time = acp_times.open_time(km, brevet_dist, date_time)
    close_time = acp_times.close_time(km, brevet_dist, date_time)
    result = {'open': open_time, 'close': close_time}

    global number
    item_doc = {
        'number': number,
        'km': km,
        'mile': mile,
        'distance': brevet_dist,
        'begin_date': begin_date,
        'begin_time': begin_time,
        'open_time': arrow.get(open_time).format('YYYY-MM-DD HH:mm'),
        'close_time': arrow.get(close_time).format('YYYY-MM-DD HH:mm'),
    }
    item_list.append(item_doc)
    number += 1

    return flask.jsonify(result=result)

@app.route('/new', methods=['POST'])
def new():
    if len(item_list) == 0:
        return redirect(url_for('wrong'))

    db.tododb.delete_many({})
    db.tododb.insert_many(item_list)

    if item_list[-1]['km'] < item_list[-1]['distance']:
        del item_list[:]
        number = 1
        return redirect(url_for('wrong'))

    del item_list[:]
    number = 1

    return redirect(url_for('index'))

@app.route('/wrong')
def wrong():
    _items = db.tododb.find()
    items = [item for item in _items]
    db.tododb.delete_many({})

    return render_template('wrong.html', items=items[-1])

@app.route('/display', methods=['POST'])
def display():
    _items = db.tododb.find()
    items = [item for item in _items]
    return render_template('display.html', items=items)


@app.route('/listAll', methods=['GET'])
@app.route('/listAll/json', methods=['GET'])
@app.route('/listAll/csv', methods=['GET'])
@app.route('/listOpenOnly', methods=['GET'])
@app.route('/listOpenOnly/json', methods=['GET'])
@app.route('/listOpenOnly/csv', methods=['GET'])
@app.route('/listCloseOnly', methods=['GET'])
@app.route('/listCloseOnly/json', methods=['GET'])
@app.route('/listCloseOnly/csv', methods=['GET'])
def list_data():
    auth_token = request.args.get('auth_token')

    if auth_token == None or verify_auth_token(auth_token) == None:
        return flask.jsonify({"message": "ERROR: Unauthorized"}), 401

    url_list = str(request.url_rule).split('/')
    del url_list[0]

    top = request.args.get('top')
    open_flag, close_flag, csv_flag = check_show_what(url_list)

    file_format = print_api(open_flag, close_flag, csv_flag, top)

    if csv_flag:
        return file_format

    return flask.jsonify(file_format)


def check_show_what(url_list):
    open_flag = False
    close_flag = False
    csv_flag = False

    if url_list[0] == 'listOpenOnly':
        open_flag = True
    elif url_list[0] == 'listCloseOnly':
        close_flag = True
    else:
        open_flag = True
        close_flag = True

    if len(url_list) > 1 and url_list[1] == 'csv':
        csv_flag = True

    return open_flag, close_flag, csv_flag

def print_api(o_flag, c_flag, csv_flag, top):
    items = list(db.tododb.find())
    open_time = []
    close_time = []
    doc = {}

    num = 0
    # csv format
    if csv_flag:
        doc = ''
        if o_flag:
            doc += 'open_time, '
        if c_flag:
            doc += 'close_time, '
        doc += '\n'
        for item in items:
            if o_flag:
                doc += item['open_time'] + ', '
            if c_flag:
                doc += item['close_time'] + ', '
            doc += '\n'
            num += 1
            if top != None and num == int(top):
                break
        return doc[:-3]


    # json format
    for item in items:
        if o_flag:
            open_time.append(item['open_time'])
        if c_flag:
            close_time.append(item['close_time'])
        num += 1
        if top != None and num == int(top):
            break
    if o_flag:
        doc['open_time'] = open_time
    if c_flag:
        doc['close_time'] = close_time

    return doc



app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print('Opening for global access on port {}'.format(CONFIG.PORT))
    app.run(port=int(CONFIG.PORT), host='0.0.0.0', debug=True)
